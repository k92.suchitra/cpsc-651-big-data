package stubs;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.RelationalGroupedDataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.internal.Logging;


import org.apache.hadoop.fs.Path;

import static org.apache.spark.sql.functions.*;

public class VehicleTypeViolation {
public static void main(String[] args) {
SparkSession session = SparkSession.builder().appName("VehicleTypeViolation").getOrCreate();
Dataset<Row> dataset = session.read().option("header","true").csv("s3://bigdata-1067764/*.csv");
//.toDF("Summons Number","Plate ID","Registration State","Plate Type","Issue Date","Violation Code","Vehicle Body Type","Vehicle Make","Issuing Agency","Street Code1","Street Code2","Street Code3","Vehicle Expiration Date","Violation Location","Violation Precinct","Issuer Precinct","Issuer Code","Issuer Command","Issuer Squad","Violation Time","Time First Observed","Violation County","Violation In Front Of Or Opposite","House Number","Street Name","Intersecting Street","Date First Observed","Law Section","Sub Division","Violation Legal Code","Days Parking In Effect","From Hours In Effect","To Hours In Effect","Vehicle Color","Unregistered Vehicle?","Vehicle Year","Meter Number","Feet From Curb","Violation Post Code","Violation Description","No Standing or Stopping Violation","Hydrant Violation","Double Parking Violation","Latitude","Longitude","Community Board","Community Council ","Census Tract","BIN","BBL","NTA");
RelationalGroupedDataset grouped=dataset.groupBy(col("Violation Code"));
Dataset<Row> dataset1= grouped.agg(count("Violation Code").as("Violation count")).orderBy(col("Violation count").desc());
dataset1.coalesce(1).write().format("csv").option("header","true").mode("Overwrite").save("s3://bigdata-1067764/OUTPUT/SPARK/PERF_OUTPUT");
}
}